#!/bin/bash
# ~/.bashrc
#
# Export editor

export EDITOR=nvim
export VISUAL=nvim
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '

# VPN Aliases
alias vpnon='protonvpn-cli c -f'
alias vpnoff='protonvpn-cli d'

# Nvim alias
alias n="nvim"
alias ndiff="nvim -d"

# Tmux alias

alias tmx="bash $HOME/.scripts/tmux.sh"
# Add cargo directory to PATH
export PATH="$HOME/.cargo/bin:$PATH"

# Add scripts directory to PATH
export PATH="$HOME/.scripts:$PATH"
export PATH="$HOME/.local/bin:$PATH"
# Initiate git ssh agent
eval "$(ssh-agent -s)" > /dev/null
ssh-add ~/.ssh/id_rsa_github 2> /dev/null

# Alias for openmw portmod mod manager
alias omw="portmod openmw"

# Starship prompt config entry
eval "$(starship init bash)"
export STARSHIP_CONFIG=~/.config/starship.toml

#Bashtop
alias btop="~/.repos/bashtop/bashtop"

#Fetch
alias fetch="~/.repos/pfetch/pfetch"

#dnf aliases
alias di="sudo dnf install"
alias dr="sudo dnf remove"
alias ds="sudo dnf search"
alias du="sudo dnf upgrade"

#i3 config alias
alias i3c="nvim ~/.config/i3/config"

#Go Learning directory shortcut
alias gost="cd ~/go/gostudy/"
