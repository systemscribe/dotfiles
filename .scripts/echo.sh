#!/bin/bash

echo "${blue}::${reset} Entering ${blue}tmux${reset} session..."
echo ""
echo "${blue}::${reset} Use ${blue}Ctrl+d${reset} to activate prefix"
echo "${blue}::${reset} Enter ${blue}h${reset} to split session horizontally"
echo "${blue}::${reset} Enter ${blue}v${reset} to split session vertically"
