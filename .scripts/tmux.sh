#!/bin/bash
#
blue=`tput setaf 4`
reset=`tput sgr0`

cat <<EOF > tmux-tmp.sh
clear
echo "${blue}::${reset} Entering ${blue}tmux${reset} session..."
echo ""
echo "${blue}::${reset} Use ${blue}Ctrl+d${reset} to activate prefix"
echo "${blue}::${reset} Enter ${blue}h${reset} to split session horizontally"
echo "${blue}::${reset} Enter ${blue}v${reset} to split session vertically"
EOF


tmux new-session -d -s mySesh -n myWin
tmux send-keys -t mySesh:myWin "bash tmux-tmp.sh" Enter
tmux attach -t mySesh:myWin
rm tmux-tmp.sh

