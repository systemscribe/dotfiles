#!/bin/bash

# Copy relevant configs to folder

homeDest="$HOME/.repos/dotfiles/"
confDest="${homeDest}.config"
scrDest="${homeDest}.scripts"
fontDest="${homeDest}.fonts"
iconDest="${homeDest}.icons"
backDest="${homeDest}.backgrounds"

mkdir -p ${homeDest} ${confDest} ${scrDest} ${fontDest} ${iconDest}
# Copy processes
cp -r $HOME/.config/i3/ ${confDest}/
cp -r $HOME/.config/i3status ${confDest}/
cp -r $HOME/.config/dunst ${confDest}/
cp -r $HOME/.config/bashtop ${confDest}/
cp $HOME/.config/picom.conf ${confDest}/
cp $HOME/.config/starship.toml ${confDest}/
#cp -r $HOME/.config/nvim ${confDest}/
cp -r $HOME/.config/alacritty ${confDest}/
cp -r $HOME/.config/polybar ${confDest}/
cp $HOME/.bashrc ${homeDest}
cp $HOME/.tmux.conf ${homeDest}
cp $HOME/.Xkbmap ${homeDest}
cp -r $HOME/.scripts/ ${homeDest}
cp -r $HOME/.backgrounds/ ${backDest}
#cp -r "$HOME/.fonts/terminalFonts/Terminus/" ${fontDest}/
#cp -r $HOME/.icons/Flatery $HOME/.icons/Flatery-Gray-Dark ${iconDest}
#cp -r $HOME/.themes/Flat-Remix-GTK-ZENWRITTEN-Darkest ${themeDest}/zdarkest

rm -rf ${confDest}/nvim/.git

#Git segment
cd ${homeDest}
git add .
git commit -m "updated configs"
git push origin main

cd $HOME/.config/nvim/lua/user/
git add .
git commit -m "updated configs"
git push origin main
