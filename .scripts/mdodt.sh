#!/bin/bash

mdIn=$1
mdOut="$2"

convertFile(){
  pandoc ${mdIn} -f markdown -t html -s -o ${mdOut}.html 
  pandoc ${mdOut}.html -f html -t odt -o ${mdOut}
}


# Check if both variables have values (non-empty)
if [[ -n "${mdIn}" && -n "${mdOut}" ]]; then
  echo ":: Creating $mdOut.odt from $mdIn..."
  convertFile
else
  echo ":: One or both variables are empty."
  echo ":: Usage is mdodt <file>.md <file>.odt"
  exit 1
fi
